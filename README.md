# The fifth practice on databases


## Task
In any programming language, implement a program to work with 
the key-value structure through a terminal.
Commands to implement

<table>
  <tr>
    <th>Team</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>SELECT n<br>where n is a number from 0 to 7</td>
    <td>Selection of the database to work with. By default, work is carried out with a database with index 0, but you can choose the neighboring one. When changing the database, the data of the previous database should be saved, but not output within the selected database</td>
  </tr>
  <tr>
    <td>KEYS *</td>
    <td>Get a list of all keys by template. <br>Template examples:<br>* - all keys,<br>key:* - key starting with key:,<br>*key* - key containing key.</td>
  </tr>
  <tr>
    <td>SET key value</td>
    <td>Set the value by key.<br>key – key, <br>value – value.<br>If such a key already exists, the value is overwritten.</td>
  </tr>
  <tr>
    <td>GET key</td>
    <td>Get the value by key.<br>key – the key.<br>If the value does not exist, null is returned.</td>
  </tr>
  <tr>
    <td>DEL key</td>
    <td>Delete the key and the value by key.<br>key – the key.<br>If there is no value, 0 is returned, if successful, 1 is returned.</td>
  </tr>
  <tr>
    <td>FLUSHALL</td>
    <td>Delete all keys and values from the current database.</td>
  </tr>
</table>

**Additional information:**
1. Commands are case-insensitive, i.e. get, GeT, and so on are considered valid
commands and will be executed. Keys and values are case-sensitive.
2. The key type is a string, the stored data type is a string. If an int is entered, for example, 
it is first converted to a string and then written as a key or value. 
3. The system must be resilient to failures, so periodically (the frequency
is set by a constant or the number of changes, or the period of time when you need
to save (for example, every 30 seconds)) data should be written to a file in order to
after restarting, the system could load the data back into memory.


## Running the code

``
python3 main.py
``

The code runs and creates a folder ``table_my_bd`` it will store from 0 to 7 files with a json dictionary. 
The code only works on Unix-like systems, since it uses *signal* for autosave in order not to lose data.
