import my_bd 

def printError(text, color = "\033[31m{}\033[0m"):
    print(color.format(text))
    return 

def printHelp():
    print("""Usage: command [arguments]\n
List of commands:
  SELECT    Selection of the database to work with. By default, 
            work is carried out with a database with an index of 0.
  KEYS      Get a list of all keys by template.
  SET       Set the value by key.
  GET       Get the value by key.
  DEL       Delete key and value by key.
  FLUSHALL  Delete all keys and values from the current database.
  HELP      What commands exist.
  QUIT      Exiting the program.""")

CLEAR_LINE = "\x1b[2K\r>>"

class Interface:
    def __init__(self) :
        self.bd = my_bd.My_Bd()
        self.loop = True
        self.list_commands = {"SELECT" : 2, 
                              "KEYS" : 2, 
                              "SET" : 3, 
                              "GET" : 2, 
                              "DEL" : 2, 
                              "FLUSHALL" : 1,
                              "HELP" : 1,
                              "QUIT": 1}

    def start(self) :
        print("""This is the implementation of the practice on the Databases of
the 2nd year student KMBO-02-21 pbalykov.
Enter "HELP" for more information.""")
        while ( self.loop ) :
            try:
                self._run_commands(input(">> "))
            except EOFError:
                self.bd.exit()
                self.loop = False
                print()
            except KeyboardInterrupt:
                self.bd.exit()
                self.loop = False
                print(CLEAR_LINE)
        return None

    def _run_commands(self, string) :
        arr = string.split()
        commands = arr[0].upper()
        if not (commands in self.list_commands): 
            printError("Error: You can get additional information by using the \"HELP\" command.")
            return 
        if len(arr) != self.list_commands[commands] :
            printError("Error: there is no such command")
            return
        match commands :
            case "SELECT":
                try:
                    if not(self.bd.select(int(arr[-1]))) :
                        printError("Error: there is no such table")
                except ValueError:
                    printError("Error: the SELECT argument must be an integer")
            case "KEYS":
                ans = self.bd.keys(arr[-1])
                for i in ans:
                    print(i)
            case "SET":
                if '*' in arr[1]:
                    printError("Error: you cannot add a key with such a character \"*\"")
                else :
                    self.bd.set(arr[1], arr[-1]) 
            case "GET":
                ans = self.bd.get(arr[-1]) 
                print(ans if ans  else "null")
            case "DEL":
                print(self.bd.del_key(arr[-1]))
            case "FLUSHALL":
                self.bd.flushall()
            case "HELP":
                printHelp()
            case "QUIT":
                self.bd.exit()
                self.loop = False
        return None
