import json, os, os.path, signal

class My_Bd:
    def __init__(self) :
        self.name_folder = os.path.dirname(__file__) + "/table_my_bd"
        self.number_bd = 8
        self.arr_table = self._open_file()
        self.index_current_table = 0
        self.current_table = self.arr_table[self.index_current_table]
        self.save = None
        self.time_save = 30
        self.time_stop = 0
        signal.signal(signal.SIGALRM, self._signal_save_file)
        signal.alarm(self.time_save)
        self.mack = {signal.SIGALRM}

    def exit(self):
        signal.alarm(self.time_save)
        self._save_file()
        return None

    def _signal_save_file(self, *value_sig) :
        signal.alarm(self.time_stop)
        self._save_file()
        signal.alarm(self.time_save)
        return None

    def _open_file(self) :
        if not(os.path.exists(self.name_folder)) :
            os.mkdir(self.name_folder)
        arr = []
        for i in range(self.number_bd) :
            file_path = self.name_folder + "/" + str(i) + ".json"
            if not(os.path.isfile(file_path)) :
                with open(file_path, "w") as file: file.write("{}")
                arr.append({})
            else:
                with open(file_path, "r") as file: arr.append(json.loads(file.read()))
        return arr
    
    def _save_file(self) :
        if not(self.save) :
            return None
        name_file = str(self.index_current_table) + ".json"
        file_path = self.name_folder + "/" + name_file
        with open(file_path, "w") as file: file.write(self.save)
        self.save = None
        return None
    
    def select(self, n) :
        if  -1 < n and n > self.number_bd :
           return False 
        signal.alarm(self.time_stop)
        self.save = json.dumps(self.current_table)
        self._save_file()
        self.index_current_table = n
        self.current_table = self.arr_table[n]
        signal.alarm(self.time_save)
        return True
    
    def keys(self, str_key) :
        arr_value = str_key.split('*')
        ans = []
        for i in self.current_table:
            if not(i.startswith(arr_value[0]) and i.endswith(arr_value[-1])) :
                continue
            beg = len(arr_value[0])
            end = len(i) - len(arr_value[-1])
            for j in range(1, len(arr_value) - 1) :
                if ( (new_x := i.find(arr_value[j], beg, end)) == -1) :
                    break
                else:
                    beg = new_x + len(arr_value[j])
            else:
                ans.append(i)
        return ans

    def set(self, key, value) :
        self.current_table[key] = value
        signal.pthread_sigmask(signal.SIG_BLOCK, self.mack)
        self.save = json.dumps(self.current_table)
        signal.pthread_sigmask(signal.SIG_UNBLOCK, self.mack)
        return None

    def get(self, key) :
        return self.current_table[key] if key in self.current_table else None

    def del_key(self, key) :
        if key in self.current_table:
            del self.current_table[key]
            signal.pthread_sigmask(signal.SIG_BLOCK, self.mack)
            self.save = json.dumps(self.current_table)
            signal.pthread_sigmask(signal.SIG_UNBLOCK, self.mack)
            return 1
        return 0
    
    def flushall(self) :
        signal.alarm(self.time_stop)
        self.current_table = {}
        self.save = json.dumps(self.current_table)
        self._save_file()
        signal.alarm(self.time_save)
        return None
