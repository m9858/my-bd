import interface

def main():
    bd = interface.Interface()
    bd.start()
    return 0

if __name__ == "__main__" :
    exit(main())
